const mainColors = {
  black1: '#112340',
  black2: 'rgba(0, 0, 0, 0.5)',
  grey1: '#7D8797',
  grey2: '#B1B7C2',
  dark1: '#112340',
  dark2: '#495A75',
  red1: '#E06379',
  blue1: '#2F80ED',

};

export const colors = {
  warning: mainColors.red1,
  background: {
    primary: 'white',
    secondary: mainColors.blue1,
    bottomNavigation: mainColors.dark1,
  },

  text: {
    primary: 'white',
    secondary: mainColors.blue1,
  },
  lineTextInput: mainColors.blue1,
  loadingBackground: mainColors.black2,
  menuInactive: mainColors.dark2,
  menuActive: mainColors.blue1,
  outlineInput: mainColors.grey2,

};
