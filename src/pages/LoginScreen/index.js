import analytics from '@react-native-firebase/analytics';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import React, { useEffect, useState } from 'react';
import {
  Alert, ImageBackground, ScrollView, StyleSheet, Text, View,
} from 'react-native';
import { AccessToken, LoginManager } from 'react-native-fbsdk-next';
import TouchID from 'react-native-touch-id';
import { useDispatch, useSelector } from 'react-redux';
import { BcLogin } from '../../assets';
import {
  ButtonComponent, IconButton, Input, LinkComponent,
} from '../../component';
import { login, signInSocialMedia } from '../../config';
import { setLoading } from '../../redux';
import {
  colors, fonts, getData, onLogScreenView, showError, showSuccess, storeData, useForm,
} from '../../utils';
import { windowHeight, windowWidth } from '../../utils/Dimensions';

export default function LoginScreen({ navigation }) {
  const stateGlobal = useSelector((state) => state);
  const dispatch = useDispatch();
  const [form, setForm] = useForm({
    email: '',
    password: '',
  });

  const [isLogin, setIsLogin] = useState(false);
  useEffect(() => {
    onLogScreenView('LoginScreen');
    GoogleSignin.configure({
      webClientId: '403463518858-h1r8hbqvojuqv2mqmcastdssddld79e1.apps.googleusercontent.com',
    });

    getData('user').then((user) => {
      if (user !== null) {
        setIsLogin(true);
      } else {
        setIsLogin(false);
      }
    });
  }, []);

  const onFingerPrintPress = () => {
    const optionalConfigObject = {
      title: 'Authentication Required',
      imageColor: '#e00606',
      imageErrorColor: '#ff0000',
      sensorDescription: 'Touch sensor',
      sensorErrorDescription: 'Failed',
      cancelText: 'Cancel',
      fallbackLabel: 'Show Passcode',
      unifiedErrors: false,
      passcodeFallback: false,
    };

    TouchID.isSupported(optionalConfigObject).then((biometryType) => {
      if (biometryType === 'FaceID') {
        Alert.alert('This device supports FaceID');
      } else {
        TouchID.authenticate('To access your account', optionalConfigObject)
          .then(() => {
            dispatch(setLoading(true));
            getData('user').then((user) => {
              if (user) {
                const users = {
                  email: user.email,
                  password: user.password,
                };
                login(users.email, users.password)
                  .then(() => {
                    analytics().logEvent('Login', {
                      method: 'Biometric',
                    });
                    analytics().setUserProperty('Login_with', 'Biometric');
                    dispatch(setLoading(false));
                    showSuccess('Login Success');
                    navigation.replace('MainApp');
                  }).catch((err) => {
                    dispatch(setLoading(false));
                    showError(err.message);
                  });
              } else {
                dispatch(setLoading(false));
                showError('Please Login first');
              }
            });
          })
          .catch((error) => {
            showError(error.message);
          });
      }
    });
  };

  async function onFacebookButtonPress() {
    dispatch(setLoading(true));
    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

    if (result.isCancelled) {
      showError('Login cancelled');
    }

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();

    if (!data) {
      showError('Something went wrong obtaining access token');
    }

    // Create a Firebase credential with the AccessToken
    const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

    // Sign-in the user with the credential
    return signInSocialMedia(facebookCredential);
  }

  const validateEmail = (text) => {
    setForm('email', text);
  };

  const validatePassword = (text) => {
    setForm('password', text);
  };

  const loginUser = () => {
    dispatch(setLoading(true));
    login(form.email, form.password)
      .then(() => {
        analytics().logEvent('Login', {
          method: 'email_password',
        });
        analytics().setUserProperty('Login_with', 'email_password');
        dispatch(setLoading(false));
        storeData('user', form);
        navigation.replace('MainApp');
        showSuccess('Login Success');
      })
      .catch((err) => {
        dispatch(setLoading(false));
        showError(err.message);
      });
  };

  async function onGoogleButtonPress() {
    dispatch(setLoading(true));
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return signInSocialMedia(googleCredential);
  }

  return (
    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="handled">
      <View style={styles.pages}>
        <ImageBackground source={BcLogin} resizeMode="cover" style={styles.image}>
          <View style={styles.loginWrapper}>
            <Text style={styles.title}>
              Login
            </Text>
            <Input label="Email" onChangeText={(text) => validateEmail(text)} value={form.email} visible={form.email.length <= 0} />
            <Input
              label="Password"
              onChangeText={(text) => validatePassword(text)}
              value={form.password}
              visible={form.password.length <= 0}
            />
            <View style={styles.linkWrapper}>
              <LinkComponent disable={stateGlobal.isLoading} title="Forgot Password?" size={16} align="right" color={colors.text.secondary} style={{ marginTop: 10 }} onPress={() => navigation.navigate('ForgotPasswordScreen')} />
            </View>
            <View style={styles.iconWrapper}>
              <IconButton
                type="Google"
                iconHeight={25}
                iconWidth={25}
                onPress={() => onGoogleButtonPress()
                  .then((res) => {
                    analytics().logEvent('Login', {
                      method: 'Google',
                    });
                    analytics().setUserId(res.user.uid);
                    analytics().setUserProperty('Login_with', 'Google');
                    const data = {
                      fullname: res.user.displayName,
                      email: res.user.email,
                      uid: res.user.uid,
                    };
                    dispatch(setLoading(false));
                    showSuccess('Login Sukses');
                    navigation.replace('MainApp');
                    database()
                      .ref(`users/${res.user.uid}/`)
                      .set(data);
                  })
                  .catch((err) => {
                    dispatch(setLoading(false));
                    showError(err.message);
                  })}
              />
              <IconButton
                type="Facebook"
                onPress={() => onFacebookButtonPress()
                  .then((res) => {
                    analytics().logEvent('Login', {
                      method: 'Facebook',
                    });
                    analytics().setUserId(res.user.uid);
                    analytics().setUserProperty('Login_with', 'Facebook');
                    const data = {
                      fullname: res.user.displayName,
                      email: res.user.email,
                      uid: res.user.uid,
                    };
                    dispatch(setLoading(false));
                    showSuccess('Login Sukses');
                    navigation.replace('MainApp');
                    database()
                      .ref(`users/${res.user.uid}/`)
                      .set(data);
                  })
                  .catch((err) => {
                    dispatch(setLoading(false));
                    showError(err.message);
                  })}
              />
              {
                isLogin ? (<IconButton type="Biometric" onPress={() => onFingerPrintPress()} />
                ) : (null)
              }
            </View>

          </View>
          <View style={styles.goRegisterWrapper}>
            <Text style={styles.registerTitle}>
              New Here?
              {' '}
            </Text>
            <LinkComponent disable={stateGlobal.isLoading} title="Register" color={colors.text.primary} size={16} onPress={() => navigation.replace('RegisterScreen')} />
          </View>
          <View style={styles.btnWrapper}>
            <ButtonComponent label="Login" onPress={() => loginUser()} disable={!(form.password && form.email) || stateGlobal.isLoading} />
          </View>
        </ImageBackground>
      </View>
    </ScrollView>

  );
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
  },

  image: {
    width: windowWidth,
    height: windowHeight,
  },

  loginWrapper: {
    paddingLeft: 21,
    position: 'absolute',
    top: windowHeight / 3,
    width: windowWidth - 21,
  },
  title: {
    fontSize: 36,
    fontFamily: fonts.primary[800],
    color: colors.text.secondary,
    marginBottom: 20,
  },

  btnWrapper: {
    position: 'absolute',
    bottom: 30,
    width: windowWidth / 3,
    right: windowWidth / 10,
  },

  iconWrapper: {
    marginTop: 16,
    flexDirection: 'row',
    width: windowWidth / 2,
    justifyContent: 'space-around',
  },
  linkWrapper: {
    marginTop: -30,
    alignItems: 'flex-end',
    marginLeft: windowWidth / 2,
  },
  goRegisterWrapper: {
    position: 'absolute',
    bottom: 10,
    left: 21,
    flexDirection: 'row',
  },
  registerTitle: {
    fontFamily: fonts.primary[600],
    fontSize: 16,
    color: colors.text.primary,
  },
});
