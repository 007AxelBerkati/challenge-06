import React, { useEffect, useState } from 'react';
import {
  StyleSheet, View,
} from 'react-native';
import { CameraScreen } from 'react-native-camera-kit';
import WebView from 'react-native-webview';
import { FlashAuto, FlashOff, FlashOn } from '../../assets';
import { ButtonComponent } from '../../component';
import { onLogScreenView, windowWidth } from '../../utils';

function QrScreen() {
  const [url, setUrl] = useState(null);
  const [isScan, setIsScan] = useState(true);
  const [webview, setWebview] = useState(false);
  useEffect(() => {
    onLogScreenView('QrScreen');
  }, []);

  const onQRCodeRead = (event) => {
    setIsScan(false);
    setUrl(event.nativeEvent.codeStringValue);
    setWebview(true);
  };

  return webview
    ? (
      <View style={styles.pages}>
        <WebView
          source={{ uri: url }}
        />
        {isScan ? null : (
          <ButtonComponent
            style={styles.button}
            label="Scan again?"
            onPress={() => {
              setIsScan(true);
              setWebview(false);
            }}
          />
        )}
      </View>

    ) : (
      <View style={styles.pages}>
        <CameraScreen
          scanBarcode={isScan}
          onReadCode={(event) => onQRCodeRead(event)}
          showFrame
          laserColor="red"
          frameColor="white"
          flashImages={{
            on: FlashOn,
            off: FlashOff,
            auto: FlashAuto,
          }}
        />
      </View>
    );
}

export default QrScreen;

const styles = StyleSheet.create({
  pages: {
    flex: 1,
  },
  button: {
    width: windowWidth / 2,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
});
