import LottieView from 'lottie-react-native';
import React, { useEffect, useState } from 'react';
import {
  Alert, Image, StyleSheet, Text, View,
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import MapView, { Callout, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { IconButton } from 'react-native-paper';
import Svg, { Image as ImageSvg } from 'react-native-svg';
import { loadingMaps, marker } from '../../assets';
import {
  colors,
  fonts,
  onLogScreenView, requestPermissions, windowWidth,
} from '../../utils';

const initialState = {
  latitude: null,
  longitude: null,
  latitudeDelta: 0,
  longitudeDelta: 0.05,
};

function HomeScreen() {
  const [currentPosition, setCurrentPosition] = useState(initialState);
  const [_marker, setMarker] = useState({});
  let myMap;

  useEffect(() => {
    onLogScreenView('HomeScreen');
    Geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        setCurrentPosition({
          ...currentPosition,
          latitude,
          longitude,
        });
      },
      (error) => Alert.alert(error.message),
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
    );
    requestPermissions();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderDetailMarker = () => (
    <View style={styles.detailWrapper}>
      <Image source={{ uri: _marker.url }} style={styles.imageDetail} />
      <View style={styles.detailMarker}>
        <Text style={styles.titleDetail}>{_marker.title}</Text>
        <Text style={styles.descriptionDetail}>{_marker.address}</Text>
      </View>
      <IconButton icon="delete" onPress={() => setMarker({})} />
    </View>
  );

  return currentPosition.latitude ? (
    <View style={styles.pages}>
      <MapView
        ref={(ref) => { myMap = ref; }}
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        initialRegion={currentPosition}
        showsUserLocation
      >
        {
        marker.data.map((item) => (
          <Marker
            key={item.id}
            coordinate={{
              latitude: item.latitude,
              longitude: item.longitude,
            }}
            title={item.title}
            description={item.description}
            onPress={() => {
              setMarker(item);
              myMap.fitToCoordinates([{
                latitude: item.latitude,
                longitude: item.longitude,
              }], {
                edgePadding: {
                  top: 50,
                  right: 50,
                  bottom: 50,
                  left: 50,
                },
                animated: true,
              });
            }}
          >
            <Callout tooltip>
              <View>
                <View style={styles.bubble}>
                  <Text style={styles.title}>{item.title}</Text>
                  <Svg width={125} height={120}>
                    <ImageSvg width="100%" height="100%" preserveAspectRatio="xMidYmid slice" href={{ uri: item.url }} />
                  </Svg>
                </View>
                <View style={styles.arrowBorder} />
                <View style={styles.arrow} />
              </View>
            </Callout>
          </Marker>
        ))

        }
      </MapView>
      {Object.prototype.hasOwnProperty.call(_marker, 'id') ? renderDetailMarker() : null}

    </View>
  ) : (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <LottieView source={loadingMaps} autoPlay loop />
    </View>
  );
}

export default HomeScreen;

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    width: windowWidth,
    height: '100%',
  },

  detailWrapper: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    flexDirection: 'row',
    backgroundColor: colors.background.secondary,
  },

  imageDetail: {
    width: 100,
    height: 100,
  },

  detailMarker: {
    flex: 1,
    padding: 10,
  },

  titleDetail: {
    fontSize: 16,
    fontFamily: fonts.primary[800],
    color: colors.text.primary,
  },

  descriptionDetail: {
    fontSize: 14,
    fontFamily: fonts.primary[400],
    color: colors.text.primary,
  },

  bubble: {
    alignSelf: 'flex-start',
    backgroundColor: colors.background.primary,
    borderRadius: 6,
    borderColor: colors.outlineInput,
    borderWidth: 0.5,
    padding: 15,
    width: 150,
  },

  arrow: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: colors.background.primary,
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -32,

  },

  arrowBorder: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: colors.background.secondary,
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -0.5,
  },

  title: {
    fontFamily: fonts.primary[600],
    fontSize: 16,
    marginBottom: 5,
    color: colors.text.secondary,

  },

});
