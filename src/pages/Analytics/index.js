import analytics from '@react-native-firebase/analytics';
import React from 'react';
import { Button, View } from 'react-native';

export default function Analytics() {
  return (
    <View>
      <Button
        title="Add To Basket"
        // eslint-disable-next-line no-return-await
        onPress={async () => await analytics().logEvent('basket', {
          id: 3745092,
          item: 'mens grey t-shirt',
          description: ['round neck', 'long sleeved'],
          size: 'L',
        })}
      />
    </View>
  );
}
