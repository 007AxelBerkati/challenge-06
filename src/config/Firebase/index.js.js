import auth from '@react-native-firebase/auth';

export const login = (email, pass) => auth().signInWithEmailAndPassword(email, pass);

export const register = (email, pass) => auth().createUserWithEmailAndPassword(email, pass);

export const forgetPassword = (email) => auth().sendPasswordResetEmail(email);

export const signInSocialMedia = (credential) => auth().signInWithCredential(credential);
