import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
// import Icon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/AntDesign';
import { colors, fonts } from '../../utils';

function IconTab({ isFocused, label }) {
  if (label === 'Home') return isFocused ? <Icon name="home" size={30} color={colors.menuActive} /> : <Icon name="home" size={30} color={colors.menuInactive} />;
  if (label === 'Scan QR') { return isFocused ? <Icon name="qrcode" size={30} color={colors.menuActive} /> : <Icon name="qrcode" size={30} color={colors.menuInactive} />; }

  return <Icon />;
}
function TabItem({ isFocused, onPress, label }) {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <IconTab isFocused={isFocused} label={label} />
      <Text style={styles.text(isFocused)}>{label }</Text>
    </TouchableOpacity>
  );
}

export default TabItem;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  text: (isFocused) => ({
    fontSize: 12,
    color: isFocused ? colors.menuActive : colors.menuInactive,
    fontFamily: fonts.primary[600],
    marginTop: 3,
  }),
});
