/* eslint-disable no-nested-ternary */
import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { HelperText, TextInput } from 'react-native-paper';
import { fonts, colors } from '../../utils';

function Input({
  label, value, onChangeText, visible,
}) {
  const [passwordVisible, setPasswordVisible] = useState(true);
  return (
    <View>
      {label === 'Email' ? (
        <View>
          <TextInput
            mode="outlined"
            label={label}
            value={value}
            onChangeText={onChangeText}
            activeOutlineColor={colors.lineTextInput}
            style={styles.input}
            keyboardType="email-address"
            outlineColor={colors.outlineInput}
            left={(
              <TextInput.Icon name="email" />
            )}

          />

          <HelperText type="error" visible={visible}>
            Email Required
          </HelperText>
        </View>
      ) : label === 'Password' ? (
        <View>
          <TextInput
            label={label}
            value={value}
            mode="outlined"
            onChangeText={onChangeText}
            secureTextEntry={passwordVisible}
            activeOutlineColor={colors.lineTextInput}
            outlineColor={colors.outlineInput}
            style={styles.input}
            left={(
              <TextInput.Icon name="key" />
            )}
            right={(
              <TextInput.Icon
                name={passwordVisible ? 'eye' : 'eye-off'}
                onPress={() => setPasswordVisible(!passwordVisible)}
                color={passwordVisible ? colors.background.secondary : colors.warning}
              />
              )}
          />
          <HelperText type="error" visible={visible}>
            Password Required
          </HelperText>
        </View>
      ) : label === 'Full Name' ? (
        <View>
          <TextInput
            mode="outlined"
            style={styles.input}
            label={label}
            value={value}
            onChangeText={onChangeText}
            activeOutlineColor={colors.lineTextInput}
            outlineColor={colors.outlineInput}
            left={(
              <TextInput.Icon name="account-circle" />
            )}

          />
          <HelperText
            type="error"
            visible={visible}
          >
            Full Name Required
          </HelperText>
        </View>
      ) : null}

    </View>
  );
}

export default Input;

const styles = StyleSheet.create({
  input: {
    fontFamily: fonts.primary[600],
    fontSize: 16,
  },
});
