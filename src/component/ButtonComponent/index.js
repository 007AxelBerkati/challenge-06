import React from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-paper';
import { colors, windowWidth } from '../../utils';

function ButtonComponent({
  onPress, label, disable, style,
}) {
  return (
    <Button style={{ ...styles.button, ...style }} onPress={onPress} mode="contained" disabled={disable}>
      {label}
    </Button>

  );
}

export default ButtonComponent;
const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.background.secondary,
    borderColor: colors.background.primary,
    borderWidth: 1,
    width: windowWidth / 3,
  },
});
