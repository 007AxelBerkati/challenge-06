import marker from './marker.json';
import loadingGlobal from './loadingGlobal.json';
import loadingMaps from './loadingMaps.json';

export { marker, loadingGlobal, loadingMaps };
