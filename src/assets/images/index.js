import BcLogin from './BcLogin.jpg';
import FlashOn from './flashOn.png';
import FlashOff from './flashOff.png';
import FlashAuto from './flashAuto.png';
import IcApp from './iconApp.png';

export {
  BcLogin, FlashOn, FlashOff, FlashAuto, IcApp,
};
